#
# Copyright (C) 2023 The BlissRoms Project
#
# SPDX-License-Identifier: Apache-2.0
#

# Essential Packages
# Carrier Services
PRODUCT_PACKAGES += \
    CarrierServices \

# Calculator
PRODUCT_PACKAGES += \
    CalculatorGooglePrebuilt \

# Clock
PRODUCT_PACKAGES += \
    PrebuiltDeskClockGoogle \
    GoogleClockOverlay \

# Contacts
PRODUCT_PACKAGES += \
    GoogleContacts \
    GoogleContactsOverlay \

# Dialer
PRODUCT_PACKAGES += \
    GoogleDialer \
    GoogleDialerOverlay \

# Messaging
PRODUCT_PACKAGES += \
    PrebuiltBugle \
    GoogleMessagesOverlay

# Digital Wellbeing
PRODUCT_PACKAGES += \
    WellbeingPrebuilt \
    DigitalWellbeingOverlay \

# Drive
PRODUCT_PACKAGES += \
    Drive \

# Maps
PRODUCT_PACKAGES += \
    LocationHistoryPrebuilt \
    Maps \
    GoogleLocationHistoryOverlay \

# Photos
PRODUCT_PACKAGES += \
    Photos \
    GooglePhotosOverlay \

# Search
PRODUCT_PACKAGES += \
    Velvet \
    VelvetOverlay

# SetupWizard
PRODUCT_PACKAGES += \
    GoogleOneTimeInitializer \
    GoogleRestorePrebuilt \
    PartnerSetupPrebuilt \
    SetupWizardPrebuilt \

# Turbo
PRODUCT_PACKAGES += \
    TurboPrebuilt \
    DeviceHealthServicesOverlay \
    GMSConfigSettingsOverlayTurbo \

# SetupWizard Props
PRODUCT_PRODUCT_PROPERTIES += \
    ro.setupwizard.enterprise_mode=1 \
    ro.setupwizard.rotation_locked=true \
    setupwizard.feature.baseline_setupwizard_enabled=true \
    setupwizard.enable_assist_gesture_training=true \
    setupwizard.feature.skip_button_use_mobile_data.carrier1839=true \
    setupwizard.feature.show_pai_screen_in_main_flow.carrier1839=false \
    setupwizard.feature.show_pixel_tos=false


PRODUCT_ENFORCE_RRO_EXCLUDED_OVERLAYS += vendor/gapps/overlay/static
PRODUCT_PACKAGE_OVERLAYS += vendor/gapps/overlay/static
