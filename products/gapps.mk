#
# Copyright (C) 2023 The BlissRoms Project
# Copyright (C) 2024 The XPerience Project
#
# SPDX-License-Identifier: Apache-2.0
#

# Default Packages
PRODUCT_PACKAGES += \
    ConfigUpdater \
    AndroidPlatformServices \
    GoogleContactsSyncAdapter \
    GoogleCalendarSyncAdapter \
    GoogleServicesFramework \
    libjni_latinimegoogle \
    MlkitBarcodeUIPrebuilt \
    Phonesky \
    PrebuiltGmsCoreSc \
    PrebuiltGmsCoreSc_AdsDynamite \
    PrebuiltGmsCoreSc_CronetDynamite \
    PrebuiltGmsCoreSc_DynamiteLoader \
    PrebuiltGmsCoreSc_DynamiteModulesA \
    PrebuiltGmsCoreSc_DynamiteModulesC \
    PrebuiltGmsCoreSc_GoogleCertificates \
    PrebuiltGmsCoreSc_MapsDynamite \
    PrebuiltGmsCoreSc_MeasurementDynamite \
    VisionBarcodePrebuilt \
    com.google.android.dialer.support

# RRO Overlays
PRODUCT_PACKAGES += \
    GMSCoreConfigOverlay \
    XPerienceFrameworksOverlay \
    PixelFrameworksOverlay \
    PixelSettingsOverlay \
    PixelSystemUIOverlay

# GMS Core
PRODUCT_PRODUCT_PROPERTIES += \
    ro.opa.eligible_device=true

ifeq ($(TARGET_ESSENTIAL_GAPPS),true)
$(warning Building just essential google packages)
$(call inherit-product, vendor/gapps/products/essential.mk)
endif

ifeq ($(TARGET_STOCK_GAPPS),true)
$(warning setting stock gapps package)
$(call inherit-product, vendor/gapps/products/stock.mk)
endif

$(call inherit-product, vendor/gapps/common/common-vendor.mk)
