#
# Copyright (C) 2023 The BlissRoms Project
#
# SPDX-License-Identifier: Apache-2.0
#

# Import essential packages
$(call inherit-product, vendor/gapps/products/essential.mk)

# Android Auto
PRODUCT_PACKAGES += \
    AndroidAutoStubPrebuilt \
    AndroidAutoOverlay

# Calendar
PRODUCT_PACKAGES += \
    CalendarGooglePrebuilt \

# Chrome Browser
PRODUCT_PACKAGES += \
    Chrome-Stub \
    TrichromeLibrary-Stub \
    WebViewGoogle64 \
    ScribePrebuilt

# Device Personalization
PRODUCT_PACKAGES += \
    DevicePersonalizationPrebuiltPixel2021 \
    DevicePersonalizationServicesOverlay \
    GmsConfigOverlayASI \

# Device Policy
PRODUCT_PACKAGES += \
    DevicePolicyPrebuilt

# Device Setup
PRODUCT_PACKAGES += \
    OTAConfigNoZeroTouchPrebuilt

# Files
PRODUCT_PACKAGES += \
    DocumentsUIGoogle \
    FilesPrebuilt \
    StorageManagerGoogle \
    PixelDocumentsUIGoogleOverlay \

# Flipendo
PRODUCT_PACKAGES += \
    Flipendo

# Gmail
PRODUCT_PACKAGES += \
    PrebuiltGmail \

# GBoard (LatinIME)
PRODUCT_PACKAGES += \
    LatinIMEGooglePrebuilt \

PRODUCT_PRODUCT_PROPERTIES += \
    ro.com.google.ime.bs_theme=true \
    ro.com.google.ime.theme_id=5 \
    ro.com.google.ime.system_lm_dir=/product/usr/share/ime/google/d3_lms

# Google TTS
PRODUCT_PACKAGES += \
    GoogleTTS \
    GoogleTTSOverlay

# Markup
PRODUCT_PACKAGES += \
    MarkupGoogle \

# Recorder
PRODUCT_PACKAGES += \
    RecorderPrebuilt \

# SoundPicker
PRODUCT_PACKAGES += \
    SoundPickerPrebuilt \

ifeq ($(TARGET_ENABLE_PIXEL_GOODIES),true)

# Pixel Goodies
PRODUCT_PACKAGES += \
    PixelThemesStub \
    PixelThemesStub2022_and_newer \
    PixelWallpapers2022 \
    WallpaperEmojiPrebuilt \
    PixelLiveWallpaperPrebuilt \
    WallpaperEffect \
    WallpaperPickerGoogleRelease \
    SettingsWallpaperPickerOverlay \
    Launcher3WallpaperPickerOverlay \

# Pixel SetupWizard
PRODUCT_PACKAGES += \
    PixelSetupWizard \

# Pixel Launcher
PRODUCT_PACKAGES += \
    NexusLauncherRelease \
    PixelLauncherOverlay

# Settings Intelligence
PRODUCT_PACKAGES += \
    DeviceIntelligenceNetworkPrebuilt \
    SettingsIntelligenceGooglePrebuilt \
    SettingsServicesOverlay \
    SettingsIntelligenceOverlay \


endif

ifeq ($(TARGET_ENABLE_PIXEL_GOODIES_WITHOUT_NEXUS_LAUNCHER),true)

# Pixel Goodies
PRODUCT_PACKAGES += \
    PixelWallpapers2022 \
    WallpaperEmojiPrebuilt \
    PixelLiveWallpaperPrebuilt \
    WallpaperEffect \
    WallpaperPickerGoogleRelease \
    SettingsWallpaperPickerOverlay \
    Launcher3WallpaperPickerOverlay \

# Pixel SetupWizard
PRODUCT_PACKAGES += \
    PixelSetupWizard \

# Settings Intelligence
PRODUCT_PACKAGES += \
    DeviceIntelligenceNetworkPrebuilt \
    SettingsIntelligenceGooglePrebuilt \
    SettingsServicesOverlay \
    SettingsIntelligenceOverlay \


endif

# YouTube
PRODUCT_PACKAGES += \
    YouTube \
    YouTubeMusicPrebuilt

# Sepolicy
BOARD_VENDOR_SEPOLICY_DIRS += \
    vendor/gapps/sepolicy

