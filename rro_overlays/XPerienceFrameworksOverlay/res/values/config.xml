<?xml version="1.0" encoding="utf-8"?>
<!--
/*
** Copyright 2024, The XPerience Project
**
** Licensed under the Apache License, Version 2.0 (the "License");
** you may not use this file except in compliance with the License.
** You may obtain a copy of the License at
**
**     http://www.apache.org/licenses/LICENSE-2.0
**
** Unless required by applicable law or agreed to in writing, software
** distributed under the License is distributed on an "AS IS" BASIS,
** WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
** See the License for the specific language governing permissions and
** limitations under the License.
*/
-->
<resources>

    <!-- Enable overlay for all location components. -->
    <bool name="config_enableNetworkLocationOverlay" translatable="false">true</bool>
    <bool name="config_enableFusedLocationOverlay" translatable="false">true</bool>
    <bool name="config_enableGeocoderOverlay" translatable="false">true</bool>
    <bool name="config_enableGeofenceOverlay" translatable="false">true</bool>

    <!-- Sets the Ephemeral Resolver [DO NOT TRANSLATE] -->
    <string-array name="config_ephemeralResolverPackage" translatable="false">
        <item>com.google.android.gms</item>
    </string-array>

    <!-- The package name of the default network recommendation app. -->
    <string name="config_defaultNetworkRecommendationProviderPackage" translatable="false">com.google.android.gms</string>

    <!-- Default service to enable with accessibility shortcut [DO NOT TRANSLATE] -->
    <string name="config_defaultAccessibilityService" translatable="false">com.google.android.marvin.talkback/.TalkBackService</string>

    <!-- Colon separated list of package names that should be granted Notification Listener access -->
    <string name="config_defaultListenerAccessPackages" translatable="false">com.android.launcher3:com.google.android.setupwizard:com.google.android.apps.restore:com.google.android.projection.gearhead</string>

    <!-- Default autofill service to enable [DO NOT TRANSLATE] -->
    <string name="config_defaultAutofillService" translatable="false">com.google.android.gms/.autofill.service.AutofillService</string>

    <!-- An array of packages for which notifications cannot be blocked. -->
    <string-array name="config_nonBlockableNotificationPackages" translatable="false">
        <item>com.google.android.setupwizard</item>
        <item>com.google.android.apps.restore</item>
    </string-array>

    <!-- Colon separated list of package names that should be granted DND access -->
    <string name="config_defaultDndAccessPackages" translatable="false">com.google.android.GoogleCamera:com.google.android.gms:com.google.intelligence.sense:com.google.android.settings.intelligence:com.google.android.apps.wellbeing:com.google.android.apps.safetyhub:com.google.android.dialer</string>

    <!-- The package name for the system's speech recognition service.
         This service must be trusted, as it can be activated without explicit consent of the user.
         Example: "com.android.speech/.RecognitionService"
    -->
    <string name="config_defaultMusicRecognitionService" translatable="false">com.google.android.googlequicksearchbox/com.google.android.apps.search.soundsearch.service.SoundSearchService</string>
    <string name="config_defaultOnDeviceSpeechRecognitionService" translatable="false">com.google.android.as/com.google.android.apps.miphone.aiai.app.AiAiSpeechRecognitionService</string>

    <!-- The component name for the default system rotation resolver service.
        This service must be trusted, as it can be activated without explicit consent of the user.
        See android.service.rotationresolver.RotationResolverService.
    -->
    <string name="config_defaultRotationResolverService" translatable="false">com.google.android.as/com.google.android.apps.miphone.aiai.app.AiAiRotationResolverService</string>

    <!-- The package name for the system's search ui service.
     This service returns search results when provided with an input string.

     This service must be trusted, as it can be activated without explicit consent of the user.
     If no service with the specified name exists on the device, on device search wil be
     disabled.
     Example: "com.android.intelliegence/.SearchUiService"
    -->
    <string name="config_defaultSearchUiService" translatable="false">com.google.android.as/com.google.android.apps.miphone.aiai.app.AiAiSearchUiService</string>

    <!-- The package name for the system's smartspace service.
     This service returns smartspace results.

     This service must be trusted, as it can be activated without explicit consent of the user.
     If no service with the specified name exists on the device, smartspace will be disabled.
     Example: "com.android.intelligence/.SmartspaceService"
    -->
    <string name="config_defaultSmartspaceService" translatable="false">com.google.android.as/com.google.android.apps.miphone.aiai.app.AiAiSmartspaceService</string>


    <!-- The component to be the default supervisor profile owner [DO NOT TRANSLATE] -->
    <string name="config_defaultSupervisionProfileOwnerComponent" translatable="false">com.google.android.gms/.kids.account.receiver.ProfileOwnerReceiver</string>

    <!-- The package name for the system's augmented autofill service.
         This service must be trusted, as it can be activated without explicit consent of the user.
         If no service with the specified name exists on the device, augmented autofill wil be
         disabled.
         Example: "com.android.augmentedautofill/.AugmentedAutofillService"
    -->
    <string name="config_defaultAugmentedAutofillService">com.google.android.as/com.google.android.apps.miphone.aiai.app.AiAiAugmentedAutofillService</string>

    <!-- The package name for the system's content suggestions service.
         Provides suggestions for text and image selection regions in snapshots of apps and should
         be able to classify the type of entities in those selections.
         This service must be trusted, as it can be activated without explicit consent of the user.
         If no service with the specified name exists on the device, content suggestions wil be
         disabled.
         Example: "com.android.contentsuggestions/.ContentSuggestionsService"
    -->
    <string name="config_defaultContentSuggestionsService">com.google.android.as/com.google.android.apps.miphone.aiai.app.AiAiContentSuggestionsService</string>

    <!-- The component name for the system-wide captions manager service.
         This service must be trusted, as the system binds to it and keeps it running.
         Example: "com.android.captions/.SystemCaptionsManagerService"
    -->
    <string name="config_defaultSystemCaptionsManagerService">com.google.android.as/com.google.android.apps.miphone.aiai.captions.SystemCaptionsManagerService</string>

    <!-- The component name for the system-wide captions service.
         This service must be trusted, as it controls part of the UI of the volume bar.
         Example: "com.android.captions/.SystemCaptionsService"
    -->
    <string name="config_defaultSystemCaptionsService">com.google.android.as/com.google.android.apps.miphone.aiai.captions.CaptionsService</string>

    <!-- The package name for the default system textclassifier service.
         This service must be trusted, as it can be activated without explicit consent of the user.
         Example: "com.android.textclassifier"
         If no textclassifier service with the specified name exists on the device (or if this is
         set to empty string), a default textclassifier will be loaded in the calling app's process.
         See android.view.textclassifier.TextClassificationManager.
    -->
    <string name="config_defaultTextClassifierPackage">com.google.android.as</string>

    <!-- Package name for the contacts metadata syncadapter [DO NOT TRANSLATE] -->
    <string name="metadata_sync_pacakge" translatable="false">com.google.android.gms</string>

    <!-- The package name for the system's app prediction service.
         This service must be trusted, as it can be activated without explicit consent of the user.
         Example: "com.android.intelligence/.AppPredictionService"
    -->
    <string name="config_defaultAppPredictionService" translatable="false">com.google.android.as/com.google.android.apps.miphone.aiai.app.AiAiPredictionService</string>

    <string name="config_batterySaverScheduleProvider">com.google.android.apps.turbo</string>

    <!-- The component name for the default system attention service.
         This service must be trusted, as it can be activated without explicit consent of the user.
         See android.attention.AttentionManagerService.
    -->
    <string name="config_defaultAttentionService" translatable="false">com.google.android.as/com.google.android.apps.miphone.aiai.attention.service.AiAiAttentionService</string>

    <!-- Package name for the device provisioning package. -->
    <string name="config_deviceProvisioningPackage" translatable="false">com.google.android.apps.work.oobconfig</string>

    <!-- Apps that are authorized to access shared accounts, overridden by product overlays -->
    <string name="config_appsAuthorizedForSharedAccounts" translatable="false">;com.android.vending;com.android.settings;</string>

    <!-- Wallpaper cropper package. Used as the default cropper if the active launcher doesn't
         handle wallpaper cropping.-->
    <string name="config_wallpaperCropperPackage" translatable="false">com.google.android.apps.wallpaper</string>

    <string name="default_wallpaper_component">com.android.customization/com.android.customization.picker.CustomizationPickerActivity</string>

    <string name="config_defaultContentCaptureService">com.google.android.as/com.google.android.apps.miphone.aiai.app.AiAiContentCaptureService</string>

    <string-array name="config_priorityOnlyDndExemptPackages">
        <item>com.google.android.dialer</item>
    </string-array>

    <!-- Enable the RingtonePickerActivity in 'com.android.providers.media'. -->
    <bool name="config_defaultRingtonePickerEnabled">false</bool>

    <!-- Corner radius for bottom sheet system dialogs -->
    <dimen name="config_bottomDialogCornerRadius">16.0dip</dimen>

    <!-- Corner radius of system buttons -->
    <dimen name="config_buttonCornerRadius">4.0dip</dimen>

    <!-- Corner radius of system dialogs -->
    <dimen name="config_dialogCornerRadius">28.0dip</dimen>

    <!-- Corner radius of system progress bars -->
    <dimen name="config_progressBarCornerRadius">1000.0dip</dimen>

    <!-- Default number of days to retain for the automatic storage manager. -->
    <integer name="config_storageManagerDaystoRetainDefault">60</integer>

    <!-- Component name that should be granted Notification Assistant access -->
    <string name="config_defaultAssistantAccessComponent" translatable="false">com.google.android.as/com.google.android.apps.miphone.aiai.common.notification.service.AiAiNotificationAssistantService</string>

    <!-- The package name for the system's translation service.
     This service must be trusted, as it can be activated without explicit consent of the user.
     If no service with the specified name exists on the device, translation wil be
     disabled.
     Example: "com.android.translation/.TranslationService"
    -->
    <string name="config_defaultTranslationService" translatable="false">com.google.android.as/com.google.android.apps.miphone.aiai.app.AiAiTranslationService</string>

    <!-- Package name of the on-device intelligent processor for ambient audio.
         Ambient audio is the sound surrounding the device captured by the DSP
         or the microphone. In other words, the device is continuously
         processing audio data in background. -->
    <string name="config_systemAmbientAudioIntelligence" translatable="false">com.google.android.as</string>

    <!-- Package name of the on-device intelligent processor for audio. The
         difference of 'ambient audio' and 'audio' is that in 'audio', the
         user intentionally and consciously aware that the device is recording
         or using the microphone.
         -->
    <string name="config_systemAudioIntelligence" translatable="false">com.google.android.as</string>

    <!-- Chooser image editing activity.  Must handle ACTION_EDIT image/png intents.
         If omitted, image editing will not be offered via Chooser.
         This name is in the ComponentName flattened format (package/class) [DO NOT TRANSLATE]  -->
    <string name="config_systemImageEditor" translatable="false">com.google.android.markup/com.google.android.markup.AnnotateActivity</string>

    <!-- Package name of the on-device intelligent processor for notification.
         -->
    <string name="config_systemNotificationIntelligence" translatable="false">com.google.android.as</string>

    <!-- Package name of the on-device intelligent processor for text. Examples
        include providing autofill functionality based on incoming text
        messages. -->
    <string name="config_systemTextIntelligence" translatable="false">com.google.android.as</string>

    <!-- Package name of the on-device intelligent processor for system UI
         features. Examples include the search functionality or the app
         predictor. -->
    <string name="config_systemUiIntelligence" translatable="false">com.google.android.as</string>

    <!-- Package name of the on-device intelligent processor for visual
         features. Examples include the autorotate feature. -->
    <string name="config_systemVisualIntelligence" translatable="false">com.google.android.as</string>

    <!-- Package name that will receive an explicit manifest broadcast for
         android.os.action.POWER_SAVE_MODE_CHANGED. -->
    <string name="config_powerSaveModeChangedListenerPackage" translatable="false">com.google.android.flipendo</string>


    <!-- The component name for the default system AmbientContextEvent detection service.
        This service must be trusted, as it can be activated without explicit consent of the user.
        See android.service.ambientcontext.AmbientContextDetectionService.
    -->
    <string name="config_defaultAmbientContextDetectionService" translatable="false">com.google.android.as/com.google.android.apps.miphone.aiai.app.AiAiAmbientContextDetectionService</string>

    <!-- Component name that accepts ACTION_SEND intents for requesting ambient context consent. -->
    <string translatable="false" name="config_defaultAmbientContextConsentComponent">com.google.android.as/com.google.android.apps.miphone.aiai.ambientcontext.ui.ConsentActivity</string>

    <!-- Intent extra key for the caller's package name while requesting ambient context consent.
     -->
    <string translatable="false" name="config_ambientContextPackageNameExtraKey">PACKAGE_NAME</string>

    <!-- Intent extra key for the event code int array while requesting ambient context consent. -->
    <string translatable="false" name="config_ambientContextEventArrayExtraKey">EVENT_ARRAY</string>

    <!-- The package name for the system's wallpaper effects generation service.
    This service returns wallpaper effects results.
    This service must be trusted, as it can be activated without explicit consent of the user.
    If no service with the specified name exists on the device, wallpaper effects
    generation service will be disabled.
    Example: "com.android.intelligence/.WallpaperEffectsGenerationService"
    -->
    <string name="config_defaultWallpaperEffectsGenerationService" translatable="false">com.google.android.as/com.google.android.apps.miphone.aiai.app.wallpapereffects.AiAiWallpaperEffectsGenerationService</string>

    <!-- The name of the package that will hold the app protection service role. -->
    <string name="config_systemAppProtectionService" translatable="false">com.google.android.odad</string>

    <!-- The package name of the default supervision package. -->
    <string name="config_systemSupervision" translatable="false">com.google.android.gms.supervision</string>

    <!-- Indicates whether the system wide captions service should also support
         call captioning.
    -->
    <bool name="config_systemCaptionsServiceCallsEnabled" translatable="false">true</bool>

    <!-- Component name that accepts settings intents for saved devices.
             Used by FastPairSettingsFragment. -->
    <string name="config_defaultNearbyFastPairSettingsDevicesComponent" translatable="false">com.google.android.gms/com.google.android.gms.nearby.discovery.devices.SavedDevicesSettingsActivity</string>

    <!-- Default component for QR code scanner -->
    <string name="config_defaultQrCodeComponent" translatable="false">com.google.android.gms/.mlkit.barcode.ui.PlatformBarcodeScanningActivityProxy</string>

    <!-- The name of the package that will hold the device management role -->
    <string name="config_devicePolicyManagement" translatable="false">com.google.android.apps.work.clouddpc:23962F4B43B9857638EA66F4D419518EAE282491ACDA4B94707BD59003C45E58</string>

    <!-- The name of the package that will handle updating the device management role. -->
    <string name="config_devicePolicyManagementUpdater" translatable="false">com.google.android.gms</string>

    <!-- Flag indicating if help links for Settings app should be enabled. -->
    <bool name="config_settingsHelpLinksEnabled">true</bool>

    <!-- The package name list for the system's cloudsearch service.
          This service returns cloudsearch results.
          This service must be trusted, as it can be activated without explicit consent of the user.
          If no service with the specified name exists on the device, cloudsearch will be disabled.
          Example: "com.android.intelligence/.CloudSearchService"
          config_defaultCloudSearchServices is for the multiple provider case.
    -->
    <string-array name="config_defaultCloudSearchServices">
        <item>com.android.vending/com.google.android.finsky.cloudsearch.PlayCloudSearchService</item>
        <item>com.android.chrome/com.google.android.apps.chrome.cloudsearch.ChromeCloudSearchService</item>
        <item>com.chrome.canary/com.google.android.apps.chrome.cloudsearch.ChromeCloudSearchService</item>
        <item>com.chrome.dev/com.google.android.apps.chrome.cloudsearch.ChromeCloudSearchService</item>
        <item>com.chrome.beta/com.google.android.apps.chrome.cloudsearch.ChromeCloudSearchService</item>
    </string-array>

    <!-- Whether to show weather on the lock screen by default. -->
    <bool name="config_lockscreenWeatherEnabledByDefault">true</bool>

    <!-- The package name of the dock manager app. Must be granted the
         POST_NOTIFICATIONS permission. -->
    <string name="config_defaultDockManagerPackageName" translatable="false">com.google.android.apps.nest.dockmanager.app</string>

    <!-- The prefixes of dream component names that are loggable.
         Matched against ComponentName#flattenToString() for dream components.
         If empty, logs "other" for all. -->
    <string-array name="config_loggable_dream_prefixes" translatable="false">
        <item>com.google.</item>
        <item>com.android.</item>
    </string-array>

    <!-- ComponentName of the default dream (Settings.Secure.DEFAULT_SCREENSAVER_COMPONENT) -->
    <string name="config_dreamsDefaultComponent" translatable="false">com.google.android.deskclock/com.android.deskclock.Screensaver</string>

    <!-- The component name, flattened to a string, for the default field classification service
         to  enabled for a user. This service must be trusted, as it can be activated
         without explicit consent of the user. If no field classification service with the
         specified name exists on the device, field classification will be disabled by default.
    -->
    <string name="config_defaultFieldClassificationService" translatable="false">com.google.android.as/com.google.android.apps.miphone.aiai.app.AutofillFieldClassificationService</string>

</resources>
